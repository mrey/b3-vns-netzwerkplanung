\pagenumbering{gobble}

# Lastenheft
### IT-Infrastruktur für die neue Büroräume der RoboSol GmbH
### Anhand der mitgeteilten Anforderungen und des Kundengesprächs haben wir Folgendes festgestellt
## Ist-Zustand

- Office und AutoCAD Lizenzen sind vorhanden
- Internetanschlusskabel liegt im Keller
- Firewall, Mail- und  Webserver stehen beim Provider
- Termin für den Bezug der Räume wird später mitgeteilt
- Kabelschächte sind nicht vorhanden
- Gebäudeänderungen / größere bauliche Maßnahmen sind nicht möglich (Denkmalschutz)
- Positionierung des Druckers ist nicht festgelegt
- Kameraüberwachung der Räume ist nicht notwendig
- Verschlüsselung, Redundanz oder Backup der Daten sind nicht notwendig


## Soll-Zustand

- Aufbau und Konfiguration einer modernen, zukunftsicheren und dienstneutralen Vernetzung
- Aufbau und Konfiguration eines Desktop-PCs für jeden Mitarbeiter mit Microsoft Windows 10
- Aufbau und Konfiguration eines Desktop-PCs mit AutoCAD und zwei Bildschirme
- Aufbau und Konfiguration eines Laptops für den Geschäftsführer mit Docking Station und einem zusätzlichen Bildschirm
- Aufbau und Konfiguration einer Präsentationsmöglichkeit im Konferenzraum
- Aufbau und Konfiguration eines gemeinsam genutzten Laserdruckers mit den folgenden Funktionen: Schwarz/Weiß, A3 Druck, Scanfunktion, Duplex
- Aufbau und Konfiguration eines zentralen benutzerfreundlichen Netzwerkspeichers
- Aufbau und Konfiguration eines Druckers im Sekretariat
- Rechner werden mindestens 5 Jahre funktionieren
- IT Support wird nur an Werktagen geleistet
- Benutzerdaten werden nur in das interne Netzwerk gespeichert und vor unberechtigtem Zugriff geschützt (DSGVO)
