\pagenumbering{gobble}

# Protokoll

## Beantworten der Fragen

   1. Wie viele Mitarbeiter?\
      7 Mitarbeiter.

   2. Was ist ein Wirtschaftsraum?\
      Neubezug der Räumlichkeit. Momentan unbenutzt. Für Arbeits- und Reinigungsmitteln gedacht.

   3. Brandschutzprobleme → Gebäudeänderung?\
      Unmöglich, da Denkmalschutz.

   4. Dienstneutrale Verkabelung?\
      Ausführen verschiedener Dienste möglich.

   5. Budget?\
      Freie Hand, aber in Konkurrenz mit den anderen Mitbewerbern. Preis/Leistung-Verhältnis. Begründen warum was verwendet wurde.

   6. Generelle Verwendung der Arbeitsstationen? Win7 noch notwendig?\
      Win 10 auch möglich. Angebotserstellung, Ausarbeitung. Bei allen ausßer 1 Rechner. Da läuft AutoCAD.

   7. Werden DSGVO eingehalten?\
      Ja, Datenschutz wird eingehalten.

   8. Backupsysteme für Rechner notwendig?\
      Zentrales System vorhanden, primär noch nicht notwendig.

   9. Sind Redundanzen gewünscht?\
      Nein, alles gut.

   10. Sind die positionen schon eingeplant?\
       Nein, Kabel sind im Keller.

   11. Wo stehen die Rechner?\
       Wahrscheinlich an der Fensterwand.

   12. Positionierung der Zentrale, Drucker?\
       Bisher unbekannt. Vorschläge gerne gesehen.
       Vielleicht nicht in einem Büro.

   13. Netzwerkanschluss in der Küche gewünscht?\
       Wenn sinnvoll, optional.

   14. Medienstation mit Displays notwendig?\
       Nein. Aber im Konferenzraum eine Präsentationsmöglichkeit wäre schön. Ein Rechner mit dem man präsentieren kann wäre gut. Und mobil sein.

   15. Brauchen Mitarbeiter mobile PCs?\
       Normale PCs sind bisher passend.

   16. Existieren bereits Kabelschächte?\
       Bisher noch nicht. Aber wir dürfen keine größeren baulichen Maßnamen machen. Wandloch okay, Boden aufreißen nicht.

   17. Inwiefern sollen die Rechner aufgesetzt werden?\
       Komplett.

   18. Zugriffsrechtseinteilung?\
       Mitarbeiter selbst hat seine eigenen Daten. Zwei Laufwerke werden zum zentralen Abspeichern von Daten verwendet werden. Eins für alle, eins für wichtige Daten, wo jeder darauf zugreifen kann, aber nicht löschen.

   19. Brauchen Sie einen IT Support? Welche Response-Time?\
       An Werktagen.

   20. Machen wir einen Wartungsvertrag?\
       Optional.

   21. Firefall vorhanden?\
       Provider.

   22. Konkreten Termin für den Bezuf der Räume?\
       Termin wird später mitgeteilt. In 8 Wochen ca.

   23. Braucht die Geschäftsführung zusätzliche Hardware?\
       Geschäftsführer braucht einen mobilen Rechner mit Dockingstation.

   24. Arbeiten Mitarbeiter nur an ihren PCs?\
       Ja.

   25. Nebenkostenvoranschlag zusätzlich eine Prognose der Arbeitszeit notwendig?\
       Ja.

   26. Wie viele Bildschirme werden pro Mitarbeiter benötigt?\
       Je eins.

   27. Was wird im Archiv gelagert?\
       Büromaterial.

   28. Wird es analog oder digital gespeichert?\
       Unwichtig.

   29. Verschlüsselung?\
       Nicht notwendig.

   30. Kameraüberwachung für interne Sicherheit?\
       Nein.

   31. Rechner mit AutoCAD: Lizenz da? Wie viele Mitarbeiter und wo arbeiten sie?\
       Ja, 1, unbekannt.

   32. Drucker bei Sekretärin?\
       Ja.

   33. Welche Druckerart?\
       Schwarz/Weiß, A3 Druck, Scanfunktion, Duplex.

   34. Drucker Scanfunktion speichern oder nur kopieren?\
       Ersteres.

   35. Grundausstattung der Rechner?\
       PC sollen funktionieren + Office.

   36. Lizenzen vorhanden?\
       Office ja, Windows nein.

   37. Rechner erweiterbar?\
       Unbekannt. Sollen 5 Jahre funktionieren.

   38. Lokale Admin Rechte? Was dürfen ihre Mitarbeiter? (Domäne joined) Dürfen sie alles selbst installieren, was sie möchten?\
       Ist das notwendig? Schlagen Sie mir was vor.
       Nein. Zentrale Gruppenverwaltung wenn mögich. Aber kein extra Server.

   39. Möbellierung kaufen?\
       Nein.
