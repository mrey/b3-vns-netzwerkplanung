\pagenumbering{gobble}

# Roadmap
## deadline: 03.05.2019

* Lastenheft erstellen (Ist- und Sollzustand)
    * [x] fehlende Informationen notieren und bei Kundengespräch nachfragen
    * [x] Protokoll des Kundengesprächs führen
    * [x] Notieren des Ist-Zustands
        * Beschreibung der aktuellen Situation und Prozesse (Dokumentenanalyse, Gespräche, Beobachtung, etc.)
    * [x] Definieren des Soll-Zustands
        * Gewünschter Zustand der Funktionen und Prozesse

* Stückliste/Inventar erstellen
    * [x] Spezifische Geräte inklusive Preis und URL notieren
        * [x] PCs und Laptops
        * [x] Tastaturen und Mäuse
        * [x] Drucker
        * [x] Netzwerkkabel
        * [x] Kabelkanal, Aufbodenkanal
        * [x] Brüstungskanal
        * [x] Patchkabel, Verlegekabel
        * [x] Patchfeld
        * [x] Switches
        * [x] NAS/Server
        * [x] Steckdosen
        * [x] Serverschrank
        * [x] Projektor
        * [x] Leinwand
        * [x] Wireless Presenter
        * [x] SFP+ Kabel
        * [x] HDMI Kabel
    * [x] kurze Begründung für die Auswahl der Komponenten angeben
    * [x] kurze Angabe über die Art der Realisierung
    * [x] Angabe von vergleichbaren Alternativen

* [x] logischen Netzwerkplan erstellen
* [x] Verkabelungsplan erstellen

* [x] Konfigurationsübersicht des Netzwerks
    * [x] Computerspezifische Konfiguration (Namen, Adressen, Freigaben, etc.)
    * [x] Benutzerspezifische Konfiguration (Benutzerdaten, Rechte, etc.)

* [x] Erstellen eines Angebots
    * [x] realistische Preise für Komponenten
    * [x] 100€/Arbeitsstunde
