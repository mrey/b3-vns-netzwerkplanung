\pagenumbering{gobble}

# Konfigurationsübersicht
Die Konfiguration der Rechner basiert auf der Annahme, dass Benutzeridentifikationen nach einem festgelegtem Namensschema generiert werden: erster Buchstabe des Vornamens + Nachname.
(Max Mustermann → mmustermann)

Die Passwörter der Nutzer werden zufällig generiert und sollen von den Nutzern selbst geändert werden.

## Zentraler Server
### Rechner-bezogene Konfiguration
* Name `server`
* IP-Adresse `192.168.1.2`
* Freigabeart `Samba (SMB)`
* Freigaben
    * `geteilt` - Zugriff über Gast-Benutzer
    * `Benutzernamens-Schema` - für jeden Benutzer eigener Zugang mit separat festgelegtem zufälligem Passwort

 
### Benutzerspezifische Konfiguration
* Benutzerkonto `Administrator` Passwort `zufällig`
* Software `QTS NAS OS`
* Administrationsschnittstelle `Webinterface auf 192.168.1.2`


## Mitarbeiterrechner 1
### Rechner-bezogene Konfiguration
* Name `Mitarbeiter1`
* IP-Adresse `192.168.1.11`
* Freigaben `keine`

### Benutzerspezifische Konfiguration
* Benutzerkonto `Benutzernamens-Schema`
* Benutzerpasswort `zufällig`
* Software `Microsoft Office`
* Speicherort von Benutzerdaten ` \\server\Benutzernamens-Schema` (192.168.1.2); Passwort `zufällig`\
→ Einbindungspunkt `Laufwerk "X"`

* Speicherort von gemeinsam genutzten Daten `\\server\geteilt` (192.168.1.2)\
→ Einbindungspunkt `Laufwerk "Y"`

## Mitarbeiterrechner 2
### Rechner-bezogene Konfiguration
* Name `Mitarbeiter2`
* IP-Adresse `192.168.1.13`
* Freigaben `keine`

### Benutzerspezifische Konfiguration
* Benutzerkonto `Benutzernamens-Schema`
* Passwort `zufällig`
* Software `Microsoft Office`
* Speicherort von Benutzerdaten ` \\server\Benutzernamens-Schema` (192.168.1.2); Passwort `zufällig`\
→ Einbindungspunkt `Laufwerk "X"`
* Speicherort von gemeinsam genutzten Daten `\\server\geteilt` (192.168.1.2)\
→ Einbindungspunkt `Laufwerk "Y"`

## Mitarbeiterrechner 3
### Rechner-bezogene Konfiguration
* Name `Mitarbeiter3`
* IP-Adresse `192.168.1.14`
* Freigaben `keine`

### Benutzerspezifische Konfiguration
* Benutzerkonto `Benutzernamens-Schema`
* Benutzerpasswort `zufällig`
* Software `Microsoft Office`
* Speicherort von Benutzerdaten ` \\server\Benutzernamens-Schema` (192.168.1.2); Passwort `zufällig`\
→ Einbindungspunkt `Laufwerk "X"`

* Speicherort von gemeinsam genutzten Daten `\\server\geteilt` (192.168.1.2)\
→ Einbindungspunkt `Laufwerk "Y"`

## Mitarbeiterrechner 4
### Rechner-bezogene Konfiguration
* Name `Mitarbeiter4`
* IP-Adresse `192.168.1.15`
* Freigaben `keine`

### Benutzerspezifische Konfiguration
* Benutzerkonto `Benutzernamens-Schema`
* Benutzerpasswort `zufällig`
* Software `Microsoft Office`
* Speicherort von Benutzerdaten ` \\server\Benutzernamens-Schema` (192.168.1.2); Passwort `zufällig`\
→ Einbindungspunkt `Laufwerk "X"`

* Speicherort von gemeinsam genutzten Daten `\\server\geteilt` (192.168.1.2)\
→ Einbindungspunkt `Laufwerk "Y"`

## AutoCAD Rechner
### Rechner-bezogene Konfiguration
* Name `AutoCAD`
* IP-Adresse `192.168.1.16`
* Freigaben `keine`

### Benutzerspezifische Konfiguration
* Benutzerkonto `Benutzernamens-Schema`
* Benutzerpasswort `zufällig`
* Software `Microsoft Office, AutoCAD`
* Speicherort von Benutzerdaten ` \\server\Benutzernamens-Schema` (192.168.1.2); Passwort `zufällig`\
→ Einbindungspunkt `Laufwerk "X"`

* Speicherort von gemeinsam genutzten Daten `\\server\geteilt` (192.168.1.2)\
→ Einbindungspunkt `Laufwerk "Y"`

## Sekretärin
### Rechner-bezogene Konfiguration
* Name `Sekretariat`
* IP-Adresse `192.168.1.11`
* Freigaben `keine`

### Benutzerspezifische Konfiguration
* Benutzerkonto `Benutzernamens-Schema`
* Benutzerpasswort `zufällig`
* Software `Microsoft Office`
* Speicherort von Benutzerdaten ` \\server\Benutzernamens-Schema` (192.168.1.2); Passwort `zufällig`\
→ Einbindungspunkt `Laufwerk "X"`

* Speicherort von gemeinsam genutzten Daten `\\server\geteilt` (192.168.1.2)\
→ Einbindungspunkt `Laufwerk "Y"`

## Geschäftsführer
### Rechner-bezogene Konfiguration
* Name `Geschaeftsleitung`
* IP-Adresse `192.168.1.10`
* Freigaben `keine`

### Benutzerspezifische Konfiguration
* Benutzerkonto `Benutzernamens-Schema` 
* Benutzerpasswort `zufällig`
* Software `Microsoft Office`
* Speicherort von Benutzerdaten ` \\server\Benutzernamens-Schema` (192.168.1.2); Passwort `zufällig`\
→ Einbindungspunkt `Laufwerk "X"`

* Speicherort von gemeinsam genutzten Daten `\\server\geteilt` (192.168.1.2)\
→ Einbindungspunkt `Laufwerk "Y"`
