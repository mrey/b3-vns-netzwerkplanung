\pagenumbering{gobble}

# Glossar
* bottleneck = Knotenpunkt, welches die maximale Gesamtleistung des Systems bestimmt.   
* Cat.x = Angabe über die Geschwindigkeitskategorie X
* NAS = Network Allocated Storage; zentraler Speicher
* Präsenter = kleines händisches Gerät, meist durch Funk mit einem Computer verbunden, der ein einfaches Weiterschalten von Folien einer Präsentation ermöglicht
* Samba = frei verfügbare Software, welches das SMB Protokoll zum Zugriff auf Netzlaufwerke implementiert. Mit Windows und Linux kompatibel
* SMB = Protokoll zum Zugriff auf Netzlaufwerke
* UI = User Interface; Bedienoberfläche
