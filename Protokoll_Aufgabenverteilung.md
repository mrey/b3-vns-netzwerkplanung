\pagenumbering{gobble}

# Alle
* 12.02.2019
    * Recherche der Geräte
    * Erstellen eines Plans zur Lokalisierung von Geräten und Komponenten
* 26.04.2019
    * Erstes Abschätzen der Kosten für das Endangebot
* 02.05.2019
    * Notieren von möglichen Alternativen zur Inventarauswahl

# Elisei
* 12.02.2019
    * Schreiben der Geräteliste
* 26.03.2019
    * Schreiben eines logischen Netzwerkplans
* 24.04.2019
    * Legende beim logischen Netzwerkplan ergänzen
* 25.04.2019
    * Recherche von Informationen für die Begründung der Inventarliste
* 26.04.2019
    * Recherche der Schreibweise eines Lastenhefts
* 30.04.2019
    * Verbesserung Ist-/Sollzustand
* 02.05.2019
    * Anpassen des Kundengesprächs

# Martin
* 05.02.2019
    * Protokollieren des Kundengesprächs
* 12.02.2019
    * Protokollführung
* 26.03.2019
    * Protokollführung
    * Niederschreiben des Roadmaps
    * Erstellen des Verkabelungsplans
* 02.04.2019
    * Protokollführung
    * Fortführen des Verkabelungsplans
    * Eintragen der recherchierten Geräte in die csv-Tabelle
* 24.04.2019
    * Protokollführung
    * Fortführen des Verkabelungsplans
* 25.04.2019
    * Protokollführung
    * Ergänzen des Verkabelungsplans mit Begründungen für Inventarauswahl
* 26.04.2019
    * Protokollführung
    * Schreiben weiterer Begründungen für die Inventarliste
* 30.04.2019
    * Protokollführung
    * Schreiben des Angebots
    * Anpassen der Inventarliste
* 02.05.2019
    * Protokollführung
    * Hinzufügen von Kabelkanälen und ergänzen der Legende des Verkabelungsplans
    * Anpassen und Neuberechnung der Arbeitskosten des Angebots
    * Umstrukturieren der Konfigurationsübersicht
    * Organiseren der Abgabemappe
    * Generieren der PDF-Dateien
    * Schreiben des Inhaltsverzeichnisses

# Leon
* 12.02.2019
    * Zusammenfassen des Kundengesprächprotokolls
    * Recherchieren möglicher NAS, Switch
* 26.03.2019
    * Recherchieren spezifischer Geräte der Inventarliste
* 02.04.2019
    * Ergänzen der Inventarliste
* 24.04.2019
    * Weiterführen der Inventarliste
    * Raumplangestaltung
* 25.04.2019
    * Recherche von Informationen für die Begründung der Inventarliste
    * Weiterführen der Inventarliste
* 26.04.2019
    * Schreiben weiterer Begründungen für die Inventarliste
* 30.04.2019
    * Notieren der Netzwerkkonfiguration
* 02.05.2019
    * Eintragen der räumlichen Verteilung des Inventars in einen Raumplan
    * Erstellen eines Dokuments zur Begründung der Inventarauswahl
    * Zusammenfügen der PDF-Dateien
    * Korrektur des Angebots
